package models;

import utils.EasyDate;

public class Person {
	
	protected Nif nif;
	protected String name;
	protected String surnames;
	protected Address address;
	protected Mail mail;
	protected EasyDate birthDate;
	
	
	
	public Person() {
		this.nif = new Nif(nif);
		this.name = new String(name);
		this.surnames = new String(surnames);
		this.address = new Address (address);
		this.mail = new Mail(mail);	
		this.birthDate = birthDate.clone();
	}

	public Nif getNif() {
		return this.nif;
	}

	public String getName() {
		return this.name;
	}

	public String getSurnames() {
		return this.surnames;
	}

	public Address getAddress() {
		return this.address;
	}

	public Mail getMail() {
		return this.mail;
	}

	public EasyDate getBirthDate() {
		return this.birthDate;
	}

	
	}
	

